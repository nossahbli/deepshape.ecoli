#!/usr/bin/env Rscript

help_output <- function() {
  "Predict in silico SHAPE score from RNA sequence. Usage: Rscript insilicoshape SEQUENCEFILE OUTPUTFILE"
}

args <- commandArgs(trailingOnly=TRUE)
if (length(args) != 2) {
   cat(help_output(), "\n")
   quit()
}
sqfile = args[1]
outfile = args[2]

if (!file.exists(sqfile)) {
  stop(paste("Cannot find sequence file:", sqfile))
}
if (file.exists(outfile)) {
   stop(paste("Output file already exists:", outfile))
}
slines = readLines(sqfile)
stopifnot(is.character(slines))

slines <- slines[grep(">",slines, invert=TRUE)] # remove header lines
sq <- paste0(slines, collapse="")

sq <- gsub("\\s+", "",sq) # remove white space

stopifnot(is.character(sq))
stopifnot(nchar(sq) > 1)
stopifnot(length(sq) == 1)

suppressPackageStartupMessages(library(rnavivo.ecoli))

data(gene_shape_data)
a <-thermo_inception_model_2_trainer(gene_shape_data,epochs=10) # train model; toy example, only use first sequences
# a <- rnn_model_4_trainer(gene_shape_data,epochs=10) # train model; toy example, only use first sequences
# a <- rnn_model_4_trainer(gene_shape_data[1:10],epochs=5) # train model; toy example, only use first sequences
result <- a(sq) # predict in-silico SHAPE scores

cat("# writing to file", outfile, "\n")
for (i in seq_along(result)) {
  cat(i, " ", result[i]," ", substr(sq,i,i), "\n", sep="",file=outfile, append=(i>1))
}
