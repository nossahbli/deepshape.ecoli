# python3 script for creating swarm file for launching test-repredict.R via Rscript
import os

wd = os.getcwd() 
outbase0 = "repredict_out"
print(wd)

for i in range(1,200):
    outbase = outbase0 + "_" + str(i)
    outfile = outbase + ".stdout"
    print("cd ",wd,"; Rscript test-repredict.R ",i," ",outbase, " >& ", outfile)
