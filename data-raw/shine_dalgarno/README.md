# Processing Translation Start Sites

In this directory, the data for translation start sites is created.
It creates or updates the data set `translationstarts` in the data directory.

## How to reproduce

`Rscript gensites.R`

Initial plotting:

`Rscript plot_sd.R`

## Who to contact

Eckart Bindewald <eckart@mail.nih.gov>

