# prepare data from mustoe 2018 paper

Solution to problem of variable number of commas in CSV file:
`cut -f1-4 -d"," transcripts.txt  > transcripts_1-4.txt`

How to reproduce:

`Rscript preprocess.R`

Quoting from original README.md file from supporting information:

```
Supporting data for Mustoe et al, Pervasive regulatory functions of mRNA structure revealed by high-resolution SHAPE probing, 2018.

transcripts.txt
--------------------------------------
Contains list of transcripts covered by the study.
Strand, genomic coordinates (U00096.2), and genes covered are listed

genes.txt
--------------------------------------
Contains list of genes covered by the study.
Genomic coordinates of the ORF (Gstart, Gstop), and transcript
coordinates of the ORF (Tstart, Tstop, 1-based), and the parent 
transcript number are listed.
```