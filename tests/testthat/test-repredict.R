library(deepshape.ecoli)
library(testthat)
# args <- commandArgs(trailingOnly=TRUE)
id <- 1
outbase <-tempfile() # "test-repredict_out"

# if (length(args) > 0) {
# id <- as.numeric(args[1])
# outbase <-paste0("test-repredict_out_",id)
# }

#if (length(args) > 1) {
# outbase = args[2]
# }

library(testthat)
library(deepshape.ecoli)
# data(shape_data)
data(mustoe18_kasugamycin, package="rnavivo.ecoli")
expect_true(!is.na(id))
shape_data <- mustoe_kasugamycin
root <- system.file(package="deepshape.ecoli")
predictor_file <- file.path(root,"data","kasugamycin_predictor.zip")
stopifnot(file.exists(predictor_file))
predictor <- load_predictor(predictor_file) # thermo_inception_model_1_trainer(shape_data[1:100],epochs=5) 
cat("Successfullly read predictor!\n")
expect_true(!is.null(predictor))
s <- shape_data[[id]]$seq
expect_is(s,"character")
result <- predictor(s)
# print(result)
expect_is(result, "matrix")
expect_equal(nchar(s),nrow(result))
# saveRDS(result,file=paste0(outbase,".rds"))
