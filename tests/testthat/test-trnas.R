suppressPackageStartupMessages(library(rnavivo.ecoli))
suppressPackageStartupMessages(library(testthat))
suppressPackageStartupMessages(library(stringr))

data(mustoe18_transcripts)
transcripts = mustoe18_transcripts
data(mustoe18_genes)
data(mustoe18_incell)
incell <- mustoe_incell  # buglet: should be mustoe18_incell

test_that("tRNA sequences can be reproduced.",{

ID <- 156
# trnalst <- mustoe18_incell[[ID]]
genes <- mustoe18_genes
trnas <- transcripts[grep("tRNA", transcripts$Gene ), ]
trnas <- trnas[trnas$Strand == "+", ]
trnas <- trnas[trnas$ID == ID, ]
genes <- genes[genes$Gene %in% trnas$Gene, ]
transcripts <- transcripts[transcripts$Gene %in% trnas$Gene, ]
# print(head(transcripts))
tlen <- unique(transcripts$Stop - transcripts$Start + 1)
trnalen <- unique(trnas$Stop - trnas$Start + 1)
# print(head(genes))
# print(trnas)
stopifnot(tlen == trnalen) # length of transcripts must match
trnalst <- list()
for (j in 1:length(incell)) {
#    print(incell[[j]])
#    print(names(incell[[j]]))
    if (nchar(incell[[j]]$seq) == tlen) {
     cat("found length match for real ID", j, "\n")
     trnalst <- incell[[j]]
     break
    }  
}
tseq <- trnalst$seq
stopifnot(is.character(tseq))
# cat("Lengths of transcripts:", nchar(tseq), tlen, "\n")
stopifnot(nchar(tseq) == tlen)
for (i in 1:nrow(genes)) {
  s <- substr(tseq, genes[i, "Tstart"], genes[i, "Tstop"])
#  cat("# working on gene", genes[i, "Gene"], genes[i, "Tstart"], "-", genes[i, "Tstop"], ":", s, "\n")  
  expect_equal(str_sub(s, -2,-1), "CA") # tRNA sequences must end with "CA"
}

# print(names(trnalst))

} ) # test_that
