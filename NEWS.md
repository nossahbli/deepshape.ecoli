
# rnavivo.ecoli 0.2.0

* Added documentation and export statements to important functions `thermo_inception_model_1_trainer` and `thermo_inception_model_1_trainer`
* Improved test cases such as `test-trnas.R`
* Added pre-trained models under inst/extdata
