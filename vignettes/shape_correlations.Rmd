---
title: "shape_correlations"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{shape_correlations}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  echo = FALSE
)
```

```{r setup}
library(deepshape.ecoli)
```

```{r message=FALSE,warning=FALSE}
supp <- suppressPackageStartupMessages
data("shape_correlations")
supp(library(deepshape.ecoli))
data("shape_correlations")
supp(library(reshape2))
mshape <- melt(shape_correlations)
supp(library(ggplot2))
supp(library(ggpubr))
ggplot(mshape,aes(x=Var2,y=value)) + geom_boxplot() + ggpubr::theme_pubclean()
```