---
title: "SHAPE Scores vs Structure Prediction"
author: "Eckart Bindewald and Noah Bliss"
date: "`r Sys.Date()`"
output:
  pdf_document
vignette: >
  %\VignetteIndexEntry{SHAPE Scores vs Structure Prediction}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```


```{r}
library(rnavivo.ecoli)
library(rnameso)
library(ggplot2)
origwd <- getwd()
if (basename(getwd())!= "rnavivo.ecoli") {
  setwd("..") # TODO changing the current working directoy in a vignette is not great
}
if (basename(getwd())!= "rnavivo.ecoli") {
    stop(paste("The current working directory has to be a the root of the source directory of the R package rnavivo.ecoli instead of ", getwd()))
}
dat <- create_SHAPE_data(progressbar = FALSE) 
d1 <- dat[[1]] # 
fold <- rna_folding(d1$seq, method="rnaplfold")[[1]]$probmatrix
d1$probsum <- colSums(fold)
d1$probmax <- apply(fold,2,FUN=max)
```

Next, we are trying a sliding-window approach in order to determine regions with strong and week correlations between SHAPE and predicted structures:
```{r}
ctab <- cor.test.window(x=d1$probsum,y=d1$score,window=50)
ctab$Probmax <- 0
for (i in 1:nrow(ctab)) {
  ctab[i, "Probmax"] <- mean((d1$probmax)[ctab[i,"Start"]:ctab[i,"End"]])
}
```

We are plotting the (negative) correlation between structure prediction and SHAPE scores for a sliding window. In red is also plotted the mean of the *maximum* probability of a region. This is aimed as distinguishing regions where the structure prediction is unambigous as opposed to regions where multiple alternative structures are possible.

```{r}
ggplot(ctab, aes(x=Start,y=-Correlation)) + geom_line() + geom_line(aes(y=Probmax), colour="red") + theme_classic(base_size=20)
setwd(origwd) # TODO eliminate workaround (move data from data-raw to inst/extdata)
```
