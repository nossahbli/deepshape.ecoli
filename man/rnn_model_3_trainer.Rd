% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/model_trainers.R
\name{rnn_model_3_trainer}
\alias{rnn_model_3_trainer}
\title{Creates, preprocesses, and trains rnn model 3}
\usage{
rnn_model_3_trainer(shape_data, epochs = 10, batch_size = 256,
  buffer_length = 100, loss_function = "logcosh", validation_split = 0.1,
  training_split = 0.1, patience = NULL)
}
\arguments{
\item{shape_data}{lists of list, with each sub list containing a string representation of the sequence and a vector of the shape scores}

\item{epochs}{number of epochs used to train the model}

\item{batch_size}{batch size used to train the model}

\item{buffer_length}{length of the leading and trailing sequences around any given neucleotide}

\item{validation_split}{fraction of transcripts to use as training validation}

\item{training_split}{fraction of transcripts to reserve for later testing}
}
\value{
returns a predictor function that takes in a sequence and returns the shape score predictions, or if you pass NULL to the predictor, it will return a list of the model used and the preprocessor
}
\description{
This function creates, preprocesses, and trains rnn model 3.
The result of this function can later be used to create predictions. (TODO: how?)
}
\examples{
\donttest{
data(gene_shape_data)
a <- rnn_model_3_trainer(gene_shape_data[1:10],epochs=5)
a("GGGGGGGGGAAAAACCCCCCCCC")
}
}
