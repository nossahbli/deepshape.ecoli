# ^^^^^^^^^^^^^^^^^^^^^^^^^^^  EDIT ME BELOW  ^^^^^^^^^^^^^^^^^^^^^^^^^^^
# Import the relevant modules
import run_unet
import sys
import pickle
import arguments
import ast

# Print the inputs to this script
print('Inputs to run_model.sh:\nParameters string: '+sys.argv[1]+'\nResult file: '+sys.argv[2])

# Save the arguments to variables
param_dic = ast.literal_eval(sys.argv[1]) # first convert the input parameters string to a Python dictionary datatype
result_path=sys.argv[2]

# .npy files that are inputs to the ML model
images_path="/home/weismanal/data/notebook/2018-10-25/files_to_run/augmented_images_5000.npy"
masks_path="/home/weismanal/data/notebook/2018-10-25/files_to_run/augmented_masks_5000.npy"

#Convert all to a string to be passed to parse_args()
arg_list = [images_path, masks_path] 
for key, value in param_dic.iteritems():
    arg_list.append("--"+key)
    arg_list.append(str(value))

# Parse the arguments for the UNET and print the final UNET input arguments
parser = arguments.get_unet_parser()
args = parser.parse_args(arg_list)
print(args)

# Run the UNET
history_callback = run_unet.evaluate_params(args) 

# Print the minimum validation loss to both the screen and to result_path
min_val = min(history_callback.history["val_loss"])
print("Minimum validation loss:")
print(min_val)
results = open(result_path, 'w')
results.write("{0}\n".format(str(min_val)))
results.close()

#Save the history as pickle object
pickle.dump(history_callback.history, open( "fit_history.p", "wb" ) ) 
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^  EDIT ME ABOVE  ^^^^^^^^^^^^^^^^^^^^^^^^^^^
