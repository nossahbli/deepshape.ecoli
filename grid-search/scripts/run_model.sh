#!/bin/bash

#A script that will run using the singularity version of swift-t...??

hostname
date

# Print the inputs to this script
echo -e "Inputs to run_model.sh:\nParameters string: $1\nEMEWS root: $2\nInstance directory: $3"

#TODO: If you would like to assign every worker a
#different GPU, then set the CUDA_VISIBLE_DEVICES as shown
#below
echo "ADLB_RANK_SELF   $ADLB_RANK_SELF"
echo "ADLB_RANK_LEADER $ADLB_RANK_LEADER"
echo "ADLB_RANK_OFFSET $ADLB_RANK_OFFSET"
export  CUDA_VISIBLE_DEVICES=$ADLB_RANK_OFFSET

set -eu

# Set param_line from the first argument to this script
# param_line is the string containing the model parameters for a run, e.g., "--nlayers=4 --conv_size=3 --activation=relu --loss_func=dice --augmentation=1.0 --num_filters=32 --last_act=sigmoid"
param_line=$1

# Set emews_root to the root directory of the project (i.e. the directory
# that contains the scripts, swift, etc. directories and files)
emews_root=$2

# Each model run, runs in its own "instance" directory
# Set instance_directory to that and cd into it.
instance_directory=$3
cd $instance_directory

# Turn bash error checking off. This is
# required to properly handle the model execution return value
# the optional timeout.
set +e

# Define the file to contain the final result for the given set of hyperparameters
result_file=$instance_directory/result.txt

which Rscript

# Run the machine learning model on the given set of hyperparameters described by $param_line, printing the result in $result_file
echo "Call of train_model.R: $emews_root/scripts/train_model.R \"$param_line\" $result_file "
$emews_root/scripts/train_model.R "$param_line" $result_file 
RES=$?

# Remove the stored weights (i.e., the optimizing parameters, not the hyperparameters)
#rm model_weights.h5

# Communicate possible errors
if [ "$RES" -ne 0 ]; then
    if [ "$RES" == 124 ]; then
	echo "---> Timeout error in model"
    else
	echo "---> Error while running model"
    fi
fi

date
