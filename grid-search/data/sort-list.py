#!/usr/local/bin/python

import csv
import sys

filename=sys.argv[1]
mycsv = csv.reader(open(filename),delimiter=',')
sortedList = sorted(mycsv, key=lambda t: float(t[0]))
string_list = [ ",".join(x) for x in sortedList ]
print "\n".join(string_list)
