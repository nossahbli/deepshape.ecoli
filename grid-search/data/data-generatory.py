
#Every attribute is a hyper parameter, with the space defined in the list of values. 
hyper_param_dict={}
hyper_param_dict['nlayers'] = ['4', '5']
hyper_param_dict['conv_size'] = ['3', '5']
hyper_param_dict['num_filters'] = ['16', '32', '64']
hyper_param_dict['activation'] = ['relu', 'tanh']
hyper_param_dict['loss_func'] = ['dice']
#hyper_param_dict['loss_func'] = ['mean_squared_error', 'binary_crossentropy', 'dice']
hyper_param_dict['last_act'] = ['sigmoid']
hyper_param_dict['augmentation'] = ['1.0', '1.5', '2.0']

def generate_sweep(attributes):
    """
        Generate all possible combinatory possibilities for the sweep. 
        Input:
            attributes: list of hyperpameters to sweep on. 
    """
    if len(attributes) == 0:
        return [""]
  
    next_param = attributes.pop()
    remaining_params =  generate_sweep(attributes)
      
    values = hyper_param_dict[next_param]
    total_params = []
    for other_param in remaining_params: 
        for value in values :
            new_param = "--" + next_param + "=" +  value
            total_params.append(other_param + " " + new_param + " ")
            
    return total_params      
        
total_sweep =  generate_sweep(hyper_param_dict.keys())
print "\n".join(total_sweep)

