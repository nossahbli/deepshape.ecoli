#!/bin/bash
instances=$(ls -d */)
#for i in `seq 1 8`
for exp in $instances
do 
    #echo instance_$i
    if [ -e $exp/out.txt ] 
    then  
        #cp instance_$i/out.txt outputs/output-$i.txt
        loss=$(tail -n 2 $exp/out.txt   | head -n 1)
        args=$(head -n 3 $exp/out.txt | tail -n 1)
        echo "$loss, $exp, $args"
    else        
        echo "Cannot find $exp"
    fi
done
