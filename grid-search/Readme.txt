
Date:   07-24-2018  
Name:   George Zaki
Email:  george.zaki@nih.gov

* This is the runnable workflow for exploring hyper-parameters for a ML model   

To Configure this model:

1- Define the machine learning model:
    a- Create a script that accepts the hyper-parameters as command line arguments
    b- Edit the TODO lines in ./scripts/run_model.sh 

2- Define the hyper-parameter space :
    a- Create a file where every line represents a hyper-paramter combination. 
       For example, take reference from: ./data/data-generator.py
       The hyperparameter combinatorial space is shown in ./data/dice-parameters.txt
    b- Edit the TODO lines in ./swift/turbine-workflow.sh

3- Define the Biowulf job:
    a- Set the Biowlf job in ./swift/sbatch-call.sh
    b- Follow the README.txt file in ./swift/README.txt to submit the job

4- Analyse the results
    In the dummy example, every model prints its loss function. 
    You can analyse all results by using the example scripts availabel in ./data
    For example, given the provided experiment "exp_example",run: 
        cd ./experiments/exp_example
        ../../data/analyze.sh  > loss-log
        python ../../data/sort-list.py loss-log

    You will see the loss for every experimnet, the hyperparameters, and the instance folder.

* See the EMEWS tutorial website at http://www.mcs.anl.gov/~emews/tutorial/
  for more information.


