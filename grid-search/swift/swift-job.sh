#!/bin/bash

#To submit the job:
module load singularity
module load openmpi/3.0.0/gcc-6.3.0

#For GPU nodes

experiment_id=${1:-experiment}
EMEWS_PROJECT_ROOT=$( cd $( dirname $0 )/.. ; /bin/pwd )
EXP_DIR=$EMEWS_PROJECT_ROOT/experiments/$experiment_id

echo "Setting experiment_id to $experiment_id"
date
mpiexec -n $SLURM_NTASKS -output-filename swift-output singularity exec --nv /data/classes/candle/candle-gpu.img swift/turbine-workflow.sh $experiment_id
date
