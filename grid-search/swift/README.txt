
To compile and submit a job using the singularity container /data/classes/candle/candle-gpu.img run the following: 

1-Compile:
Get an interactive node
Run:
    module load singularity
    singularity exec /data/classes/candle/candle-gpu.img ./stc-workflow.sh <experiment-name>


2-Run using turbine:
- Make sure the swift-job.sh singularity call includes --nv option
- Run the sbatch command:
./sbatch-call.sh <experiment-name> 
