#!/usr/bin/env bash

set -eu
hostname

# uncomment to turn on swift/t logging. Can also set TURBINE_LOG,
# TURBINE_DEBUG, and ADLB_DEBUG to 0 to turn off logging
# export TURBINE_LOG=1 TURBINE_DEBUG=1 ADLB_DEBUG=1
export EMEWS_PROJECT_ROOT=$( cd $( dirname $0 )/.. ; /bin/pwd )
# source some utility functions used by EMEWS in this script
source "${EMEWS_PROJECT_ROOT}/etc/emews_utils.sh"

export EXPID=$1
export TURBINE_OUTPUT=$EMEWS_PROJECT_ROOT/experiments/$EXPID

export PROCS=$SLURM_NTASKS
# for your run. Note that the default $* will pass all of this script's
# command line arguments to the swift script.
CMD_LINE_ARGS="$*"

# set machine to your schedule type (e.g. pbs, slurm, cobalt etc.),
# or empty for an immediate non-queued unscheduled run
MACHINE=""

if [ -n "$MACHINE" ]; then
  MACHINE="-m $MACHINE"
fi

# Add any script variables that you want to log as
# part of the experiment meta data to the USER_VARS array,
# for example, USER_VARS=("VAR_1" "VAR_2")
USER_VARS=()
# log variables and script to TURBINE_OUTPUT directory
log_script

# echo's anything following this standard out

WORKFLOW_SWIFT=ai_workflow3.swift
#George Zaki:Get the tic filename
TIC_FILE_NAME=$(echo $WORKFLOW_SWIFT | sed "s/\.swift/\.tic/")
TIC_OUTPUT=$TURBINE_OUTPUT/$TIC_FILE_NAME

set -x

#TODO set the parameters file
parameters_file="$EMEWS_PROJECT_ROOT/data/dice-params.txt"

turbine -n $PROCS $MACHINE $TIC_OUTPUT -f="$parameters_file" $CMD_LINE_ARGS
