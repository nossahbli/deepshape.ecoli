#!/usr/bin/env bash
set -eu

if [ "$#" -ne 1 ]; then
  script_name=$(basename $0)
  echo "Usage: ${script_name} EXPERIMENT_ID (e.g. ${script_name} experiment_1)"
  exit 1
fi


# uncomment to turn on swift/t logging. Can also set TURBINE_LOG,
# TURBINE_DEBUG, and ADLB_DEBUG to 0 to turn off logging
# export TURBINE_LOG=1 TURBINE_DEBUG=1 ADLB_DEBUG=1
export EMEWS_PROJECT_ROOT=$( cd $( dirname $0 )/.. ; /bin/pwd )
# source some utility functions used by EMEWS in this script
source "${EMEWS_PROJECT_ROOT}/etc/emews_utils.sh"

export EXPID=$1
export TURBINE_OUTPUT=$EMEWS_PROJECT_ROOT/experiments/$EXPID
check_directory_exists

# Add any script variables that you want to log as
# part of the experiment meta data to the USER_VARS array,
# for example, USER_VARS=("VAR_1" "VAR_2")
USER_VARS=()
# log variables and script to TURBINE_OUTPUT directory
log_script


WORKFLOW_SWIFT=ai_workflow3.swift
#George Zaki:Get the tic filename
TIC_FILE_NAME=$(echo $WORKFLOW_SWIFT | sed "s/\.swift/\.tic/")
TIC_OUTPUT=$TURBINE_OUTPUT/$TIC_FILE_NAME


# echo's anything following this standard out
set -x
stc -p $EMEWS_PROJECT_ROOT/swift/swiftrun.swift $TIC_OUTPUT
