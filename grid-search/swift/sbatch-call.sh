#!/bin/bash


experiment_id=${1:-experiment}
local_dir=`pwd`

#TODO edit the biowulf job parameters 
#ntasks should be greater than 2
ntasks=3
job_time=60
memory=10G

sbatch --output=$local_dir/../experiments/$experiment_id/output.txt --error=$local_dir/../experiments/$experiment_id/error.txt --partition=gpu --gres=gpu:k80:1 --cpus-per-task=2 --ntasks=$ntasks --mem=$memory --job-name=$experiment_id --time=$job_time  --ntasks-per-node=1 ./swift-job.sh $experiment_id 

#TODO:
#To use multiple GPU per node and assing every worker a GPU, use the following sbatch call:
#sbatch --output=$local_dir/../experiments/$experiment_id/output.txt --error=$local_dir/../experiments/$experiment_id/error.txt --partition=gpu --gres=gpu:k80:4 --cpus-per-task=2 --ntasks=$ntasks --mem=$memory --job-name=$experiment_id --time=$job_time  --ntasks-per-node=5 ./swift-job.sh $experiment_id

#NOTE, you have to set the CUDA_VISIBLE_DEVICES for every worker as shown in ../scripts/run_model.sh 
